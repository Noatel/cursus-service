package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.model.Dao.Slagingscriterium;
import nl.kzaconnected.cursus.repository.SlagingscriteriumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlagingscriteriumService {

    @Autowired
    private SlagingscriteriumRepository slagingscriteriumRepository;

    public List<Slagingscriterium> findAll(){
        return slagingscriteriumRepository.findAll();
    }
}
